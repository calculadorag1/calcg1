package calculadora;

public class Calculadora {

    public static void main(String[] args) {
        
        System.out.println("Suma: "+suma(5, 5));
        System.out.println("Resta: "+resta(6,3));
        System.out.println("Multiplicacio: "+multiplicacion(4,8));
        System.out.println("Divisió: "+division(6,2));
        System.out.println("Percentatge: "+porcentaje(20,40));
        System.out.println("Potencia: ");
        System.out.println("Arrel: "+Math.sqrt(6));
    }
    
    public static int suma(int a, int b) {
        return a + b;
    }
    
     public static int resta(int a, int b) {
        return a - b;
    }
     
     public static int multiplicacion(int a, int b) {
        return a * b;
    }
     //multiplicacio
     
     
    public static int division(int a, int b) {
        try {
            return (int) a / b;
        } catch (Exception e) {
            System.err.println("ERROR - !No se puede dividir entre cero!");
            return -1;
        }
    }
    //comentario test
    
    public static int porcentaje(int num, int per) {
        return (int) ( (num * per) / 100) ) + num;
    }
    //Comentario nuevo
    
}


